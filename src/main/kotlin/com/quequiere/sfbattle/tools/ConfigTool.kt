package com.quequiere.sfbattle.tools

import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.quequiere.sfbattle.SfbattlePlugin
import org.bukkit.Bukkit
import java.io.*


class ConfigTool {
    companion object{

        fun getGson() : Gson
        {
            return  GsonBuilder().setPrettyPrinting().create()
        }

        fun saveObject(data : Any,fileName : String, subFolder : String)
        {
            val json = getGson().toJson(data)

            val folder = getPluginFolder(subFolder)
            val file = File("${folder.path}\\$fileName.json")
            file.createNewFile()

            val writer =  BufferedWriter( FileWriter(file))

            try {
                writer.write(json)
            }catch (e : Exception) {
                e.printStackTrace()
            }
            finally {
                writer.close()
            }
        }

        fun getAllObjectNames(subFolder: String) : List<String>
        {
            val folder = getPluginFolder(subFolder)
            return folder.listFiles().mapNotNull { it.name.replace(".json","") }
        }

        inline fun <reified T : Any> loadObject(fileName: String, subFolder: String) : T
        {
            val folder = getPluginFolder(subFolder)
            val file = File("${folder.path}\\$fileName.json")
            val br = BufferedReader(FileReader(file)).readLines().joinToString("")

            return getGson().fromJson<T>(br,T::class.java)
        }

        fun getPluginFolder(child : String) : File
        {
            val dir = File(Bukkit.getPluginManager().getPlugin(SfbattlePlugin.pluginName)!!.dataFolder.toString()+"\\$child")
            if(dir.exists())
                dir.mkdirs()
            return dir
        }
    }
}