package com.quequiere.sfbattle.tools

import com.quequiere.sfbattle.SfbattlePlugin
import org.bukkit.ChatColor
import org.bukkit.entity.HumanEntity
import org.bukkit.entity.Player

fun HumanEntity.pluginMessage(message : String, color: ChatColor = ChatColor.WHITE){

    this.sendMessage("${ChatColor.GREEN}${ChatColor.BOLD}${SfbattlePlugin.pluginName} : ${ChatColor.RESET}$color$message")
}

class PluginTools {
}