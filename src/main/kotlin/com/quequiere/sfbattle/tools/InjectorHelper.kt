package com.quequiere.sfbattle.tools

import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Injector
import com.quequiere.sfbattle.SfbattlePlugin

lateinit var injector : Injector

class InjectorHelper(private val plugin: SfbattlePlugin) : AbstractModule() {

    fun createInjector(): Injector {
        return Guice.createInjector(this)
    }

    override fun configure() {
        this.bind(SfbattlePlugin::class.java).toInstance(this.plugin)
    }


    companion object {
        fun initialize(plugin: SfbattlePlugin)
        {
            val module = InjectorHelper(plugin)
            injector = module.createInjector()
           injector.injectMembers(plugin)
        }

    }
}