package com.quequiere.sfbattle.models

import net.md_5.bungee.api.ChatColor
import org.bukkit.Bukkit
import org.bukkit.Location
import java.util.*

class ArenaTeam(
    val teamname: String,
    var maxSize: Int = 20,
    private var color: ChatColor = ChatColor.WHITE,
    val players : MutableList<UUID> = mutableListOf(),
    val spawn: MutableList<Location> = mutableListOf()
) {

    fun setColor(color: ChatColor) {
        this.color = color

        this.players
            .mapNotNull { Bukkit.getPlayer(it) }
            .forEach {
                it.setDisplayName(color.toString() + it.name + ChatColor.WHITE)
                it.setPlayerListName(color.toString() + it.name)
                it.customName = color.toString() + it.name
            }
    }


}