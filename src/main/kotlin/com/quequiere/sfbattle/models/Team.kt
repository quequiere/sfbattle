package com.quequiere.sfbattle.models

import net.md_5.bungee.api.ChatColor
import org.bukkit.Location

class Team(val name : String) {

    var colors : ChatColor = ChatColor.WHITE
    var maxPlayer : Int = 20
    var teamHub : Location? = null
    val spawners : List<Spawner> = listOf()
}