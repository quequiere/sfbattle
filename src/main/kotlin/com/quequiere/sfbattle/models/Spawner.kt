package com.quequiere.sfbattle.models

import org.bukkit.Location

class Spawner(val location : Location) {

    val coolDownMS : Double = 5000.0
    val noCoolDown : Boolean = false
    val name : String = "some-Spawner"
}