package com.quequiere.sfbattle.models

import com.quequiere.sfbattle.menu.annotation.MenuOption
import com.quequiere.sfbattle.tools.ConfigTool
import org.bukkit.Bukkit
import org.bukkit.World

class Arena(val X1 : Int,
                   val Y1 : Int,
                   val Z1 : Int,
                   val X2 : Int,
                   val Y2 : Int,
                   val Z2 : Int,
                   val worldName : String,
                   val name : String) {



    @MenuOption var canPlace : Boolean = true
    @MenuOption var canInteract : Boolean = true
    @MenuOption var canBreak : Boolean = true

    @MenuOption val teamsConfig = listOf<Team>()


    fun getWorld() : World{
        return  Bukkit.getWorld(this.name)?:throw Exception("Error while loading $name arena, world $worldName doesn't exist")
    }

    fun save() {
        ConfigTool.saveObject(this,this.name,"Arenas")
    }


    companion object {
        fun getArenaFromFile(arenaName : String) : Arena {
           return ConfigTool.loadObject(arenaName,"Arenas")
        }
    }
}