package com.quequiere.sfbattle.menu.helpers

import com.quequiere.sfbattle.SfbattlePlugin
import com.quequiere.sfbattle.sfbattle
import xyz.janboerman.guilib.api.menu.MenuHolder

abstract class ParametrableObjectMenu(val tittle : String, val obj : Any) : MenuHolder<SfbattlePlugin>(sfbattle, 54, tittle) {

    init {
    }
}