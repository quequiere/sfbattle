package com.quequiere.sfbattle.menu.helpers

import org.bukkit.Material
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import xyz.janboerman.guilib.api.ItemBuilder
import xyz.janboerman.guilib.api.menu.ItemButton
import xyz.janboerman.guilib.api.menu.MenuHolder
import xyz.janboerman.guilib.api.menu.RedirectItemButton
import xyz.janboerman.guilib.api.menu.ToggleButton

fun generateIS(name : String) : ItemStack {
    return ItemBuilder(Material.STRUCTURE_VOID).name(name).build()
}

fun <T : MenuHolder<*>> generateItemButtonComplete(item: ItemStack, onclick: ((T, InventoryClickEvent) -> Unit)? ): ItemButton<T> {
    return object :  ItemButton<T>(item){
        override fun onClick(holder: T, event: InventoryClickEvent?) {
            super.onClick(holder, event)
            onclick?.invoke(holder,event!!)
        }
    }
}

fun <T : MenuHolder<*>> generateItemButton(item: ItemStack, onclick: ((InventoryClickEvent) -> Unit)? ): ItemButton<T> {
    return object :  ItemButton<T>(item){
        override fun onClick(holder: T, event: InventoryClickEvent?) {
            super.onClick(holder, event)
            onclick?.invoke(event!!)
        }
    }
}

fun <T : MenuHolder<*>> generateRedirectButton(item: ItemStack, onclick: (() -> Inventory) ): RedirectItemButton<T> {
    return RedirectItemButton(item,onclick)
}


fun <T : MenuHolder<*>> generateToggleButton(firstState : Boolean,name : String,onclick: ((InventoryClickEvent,enabled : Boolean) -> Unit)? ): ToggleButton<T> {
    return object :  ToggleButton<T>(boolItemStack(firstState,name),firstState){

        override fun afterToggle(holder: T, event: InventoryClickEvent?) {
            onclick?.invoke(event!!,this.isEnabled)
        }

        override fun updateIcon(menuHolder: T, event: InventoryClickEvent?): ItemStack {
            return boolItemStack(this.isEnabled, name)
        }
    }
}

private fun boolItemStack(bool: Boolean, name : String) : ItemStack
{
    val type = if (bool) Material.BLUE_BANNER else Material.RED_BANNER
    return ItemBuilder(type).name("$name: $bool").build()
}