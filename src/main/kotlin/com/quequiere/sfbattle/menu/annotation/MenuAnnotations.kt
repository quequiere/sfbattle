package com.quequiere.sfbattle.menu.annotation

import xyz.janboerman.guilib.api.menu.MenuButton
import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
annotation class MenuOption

fun convertClassToTypes(obj : Any) {

   val annotatedFields =  obj.javaClass.declaredFields.filter { it.isAnnotationPresent(MenuOption::class.java)}


}