package com.quequiere.sfbattle.menu.admin

import com.quequiere.sfbattle.SfbattlePlugin
import com.quequiere.sfbattle.menu.helpers.generateIS
import com.quequiere.sfbattle.sfbattle
import xyz.janboerman.guilib.api.menu.CloseButton
import xyz.janboerman.guilib.api.menu.MenuHolder
import xyz.janboerman.guilib.api.menu.RedirectItemButton


class AdminMainMenu : MenuHolder<SfbattlePlugin>(sfbattle, 9, "Admin main menu") {

    init {
        this.setButton(0, RedirectItemButton<MenuHolder<SfbattlePlugin>>(generateIS("Arena creation/modification"), AdminArenaMenu()::getInventory))
        this.setButton(8, CloseButton<SfbattlePlugin>())
    }
}


