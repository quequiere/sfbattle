package com.quequiere.sfbattle.menu.admin

import com.google.inject.Inject
import com.google.inject.Singleton
import com.quequiere.sfbattle.SfbattlePlugin
import com.quequiere.sfbattle.menu.helpers.generateIS
import com.quequiere.sfbattle.menu.helpers.generateItemButton
import com.quequiere.sfbattle.menu.helpers.generateRedirectButton
import com.quequiere.sfbattle.menu.helpers.generateToggleButton
import com.quequiere.sfbattle.services.ArenaConfigService
import com.quequiere.sfbattle.services.ArenaService
import com.quequiere.sfbattle.sfbattle
import com.quequiere.sfbattle.tools.injector
import me.McVier3ck.arena.Arena
import org.bukkit.entity.Player
import org.bukkit.event.inventory.InventoryClickEvent
import xyz.janboerman.guilib.api.menu.CloseButton
import xyz.janboerman.guilib.api.menu.MenuHolder

class AdminArenaMenu : MenuHolder<SfbattlePlugin>(sfbattle, 54, "Arena creation") {

    @Inject
    lateinit var arenaConfigService: ArenaConfigService

    @Inject
    lateinit var arenaService: ArenaService

    init {
        //Used to injection cause has been use with new ()
        injector.injectMembers(this)

        this.setButton(0,  generateItemButton<AdminMainMenu>(generateIS("Create new Arena")) { event ->
            arenaConfigService.askArenaName(event.whoClicked as Player) { arenaConfigService.createNewArena(event.whoClicked as Player,it)}
            event.whoClicked.closeInventory()
        })

//        arenaService.getArenas().forEachIndexed { index, arena ->
//            val button =  generateRedirectButton<AdminMainMenu>(generateIS("Arena: ${arena.name}"), ArenaConfigMenu(arena)::getInventory)
//            this.setButton(9+index, button)
//        }


        this.setButton(8, CloseButton<SfbattlePlugin>())
    }
}
