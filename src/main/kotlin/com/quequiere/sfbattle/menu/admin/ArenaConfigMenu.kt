package com.quequiere.sfbattle.menu.admin

import com.quequiere.sfbattle.SfbattlePlugin
import com.quequiere.sfbattle.menu.helpers.generateToggleButton
import com.quequiere.sfbattle.sfbattle
import me.McVier3ck.arena.Arena
import org.bukkit.event.inventory.InventoryClickEvent
import xyz.janboerman.guilib.api.menu.MenuHolder

class ArenaConfigMenu(val arena : Arena) : MenuHolder<SfbattlePlugin>(sfbattle, 54, "Arena ${arena.name} config") {

    init {
        this.setButton(0, generateToggleButton<AdminMainMenu>(arena.canBreak,"Set can break"){ _: InventoryClickEvent, b: Boolean ->
            arena.canBreak = b
            arena.save()
        })

        this.setButton(1, generateToggleButton<AdminMainMenu>( arena.canPlace,"Set can place"){ _: InventoryClickEvent, b: Boolean ->
            arena.canPlace = b
            arena.save()
        })

        this.setButton(2, generateToggleButton<AdminMainMenu>(arena.canInteract,"Set can interact"){ _: InventoryClickEvent, b: Boolean ->
            arena.canInteract = b
            arena.save()
        })
    }
}