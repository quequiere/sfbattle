package com.quequiere.sfbattle

import com.google.inject.Inject
import com.quequiere.sfbattle.commands.AdminCommands
import com.quequiere.sfbattle.listener.InputAskerEventWatcher
import com.quequiere.sfbattle.services.ArenaConfigService
import com.quequiere.sfbattle.tools.InjectorHelper
import me.McVier3ck.arena.ArenaListener
import org.bukkit.plugin.java.JavaPlugin
import java.util.logging.Logger

lateinit var sfbattle : SfbattlePlugin
lateinit var plogger : Logger

class SfbattlePlugin : JavaPlugin() {

    @Inject
    lateinit var inputAskerEventWatcher: InputAskerEventWatcher
    @Inject
    lateinit var arenaListener: ArenaListener




    @Inject
    lateinit var arenaConfigService: ArenaConfigService

    override fun onEnable() {
        plogger = this.logger
        logger.info("SfbattlePlugin started !")

        sfbattle = this
        InjectorHelper.initialize(this)

        this.registerCommands()
        this.registerEvents()
        this.postStartupRoutine()
    }

    private  fun registerCommands()
    {
        this.getCommand("sfadmin")!!.setExecutor(AdminCommands())

    }

    private fun registerEvents()
    {
        server.pluginManager.registerEvents(inputAskerEventWatcher, this)
        server.pluginManager.registerEvents(arenaListener, this)
    }

    private fun postStartupRoutine()
    {
        arenaConfigService.reloadArenas()
    }

    companion object {
        const val pluginName ="SFBattle"
    }
}

fun printMessage(message : String)
{
    plogger.info("[${SfbattlePlugin.pluginName}] $message")
}