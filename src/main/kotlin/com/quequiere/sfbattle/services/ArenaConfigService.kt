package com.quequiere.sfbattle.services

import com.google.inject.Inject
import com.quequiere.sfbattle.models.Arena
import com.quequiere.sfbattle.printMessage
import com.quequiere.sfbattle.tools.ConfigTool
import com.quequiere.sfbattle.tools.pluginMessage
import com.sk89q.worldedit.bukkit.WorldEditPlugin
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.entity.Player


class ArenaConfigService {

    @Inject lateinit var inputAskerService : InputAskerService
    @Inject lateinit var  arenaService : ArenaService

    fun reloadArenas()
    {
       val arenasName = ConfigTool.getAllObjectNames("Arenas")

        printMessage("Reloading all arenas ...")
        val loaded = arenasName.mapNotNull { Arena.getArenaFromFile(it) }
        printMessage("${loaded.size} arenas loaded")

        arenaService.setArenas(loaded)
    }

    fun askArenaName(player : Player, callback : (String) -> Unit )
    {
        player.pluginMessage("Please provide the new arena name in chat")
        inputAskerService.registerInputDemand(player,callback)
    }

    fun createNewArena(player : Player, name : String)
    {

        if(arenaService.getArena(name)!=null)
        {
            player.pluginMessage("Sorry, an arena already exist with this name", ChatColor.RED)
            return
        }

        val worldEdit = Bukkit.getServer().pluginManager.getPlugin("WorldEdit") as WorldEditPlugin?
        if (worldEdit == null) {
            player.pluginMessage("Error, worldedit is not loaded on this server !", ChatColor.RED)
            return
        }

        val tutorialArena : Arena

        try
        {
            val session = worldEdit.getSession(player)
            val sel = session.getSelection(session.selectionWorld)
            val X1 = sel.maximumPoint.blockX
            val Y1 = sel.maximumPoint.blockY
            val Z1 = sel.maximumPoint.blockZ
            val X2 = sel.minimumPoint.blockX
            val Y2 = sel.minimumPoint.blockY
            val Z2 = sel.minimumPoint.blockZ

            tutorialArena = Arena(X1,Y1,Z1,X2,Y2,Z2,sel.world!!.name,name)

        }catch (e : Exception) {
            player.pluginMessage("Error, while get worldEdit selection, did you select a zone ?", ChatColor.RED)
            return
        }


        tutorialArena.save()

        player.pluginMessage("You provide $name for arena name. Arena created ! You can find it in arena list.")
    }

}