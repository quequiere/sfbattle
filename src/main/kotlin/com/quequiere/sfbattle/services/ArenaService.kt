package com.quequiere.sfbattle.services

import com.google.inject.Singleton
import com.quequiere.sfbattle.models.Arena

@Singleton
class ArenaService {

    private val arenas = mutableListOf<Arena>()

    fun getArena(name : String) : Arena?{
        return arenas.firstOrNull { it.name == name }
    }

    fun setArenas(l : List<Arena>)
    {
        arenas.clear()
        arenas.addAll(l)
    }

    /**
     * To avoid the modification of the list
     */
    fun getArenas() : List<Arena>
    {
        return arenas.toList()
    }
}