package com.quequiere.sfbattle.services

import com.google.inject.Singleton
import org.bukkit.entity.HumanEntity


@Singleton
class InputAskerService{

    private val waitingInput = mutableMapOf<HumanEntity,(String) -> Unit>()


    fun registerInputDemand(player : HumanEntity, afterInput : (String) -> Unit)
    {
        waitingInput.remove(player)
        waitingInput[player] = afterInput
    }

    fun tryExecuteInput(player: HumanEntity, input: String) : Boolean{

        if(waitingInput.containsKey(player))
        {
            waitingInput[player]!!.invoke(input)
            waitingInput.remove(player)
            return true
        }
        return false
    }
}