package com.quequiere.sfbattle.listener

import com.google.inject.Inject
import com.google.inject.Singleton
import com.quequiere.sfbattle.services.InputAskerService
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent

@Singleton
class InputAskerEventWatcher : Listener {

    @Inject
    lateinit var inputAskerService: InputAskerService

    @EventHandler
    fun onChatEvent(e: AsyncPlayerChatEvent) {
        if(inputAskerService.tryExecuteInput(e.player,e.message))
        {
            e.isCancelled = true
        }
    }
}